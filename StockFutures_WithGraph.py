import pandas as pd
import matplotlib.pyplot as plt
from mpldatacursor import datacursor

FilePath = [
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo24JUN2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo23JUN2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo22JUN2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo19JUN2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo18JUN2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo17JUN2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo16JUN2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo15JUN2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo12JUN2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo11JUN2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo10JUN2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo09JUN2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo08JUN2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo05JUN2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo04JUN2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo03JUN2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo02JUN2020bhav.csv",
        ]


Total_Number_of_Files = len(FilePath)


df_futures = [None] * Total_Number_of_Files
df_Contracts_and_OIchange_complete = [None] * Total_Number_of_Files
df_Contracts_and_OIchange_slice = [None] * Total_Number_of_Files

for file_index in range(0,Total_Number_of_Files):
    #print(FilePath[file_index])
    df_futures[file_index] = pd.read_csv(FilePath[file_index])

    #df_Contracts_and_OIchange_complete[file_index]= df_futures[file_index][(df_futures[file_index]['INSTRUMENT'] == 'FUTSTK') & (df_futures[file_index]['EXPIRY_DT'] == '28-May-2020')]
    df_Contracts_and_OIchange_complete[file_index] = df_futures[file_index][(df_futures[file_index]['INSTRUMENT'] == 'FUTSTK') & (df_futures[file_index]['EXPIRY_DT'] == '25-Jun-2020')]
    #df_Contracts_and_OIchange_complete[file_index] = df_futures[file_index][(df_futures[file_index]['INSTRUMENT'] == 'FUTSTK') & (df_futures[file_index]['EXPIRY_DT'] == '30-Jul-2020')]
    df_Contracts_and_OIchange_slice[file_index]  = df_Contracts_and_OIchange_complete[file_index][['SYMBOL', 'CONTRACTS', 'CHG_IN_OI','CLOSE','OPEN_INT']]


df_semi = [None]*Total_Number_of_Files
df_semi[0] = pd.merge(df_Contracts_and_OIchange_slice[0],df_Contracts_and_OIchange_slice[1],on="SYMBOL",how="left")
df_semi[0].rename(columns={"CONTRACTS_x": "CONTRACTS_N", "CHG_IN_OI_x": "CHG_IN_OI_N","CONTRACTS_y": "CONTRACTS_N_1", "CHG_IN_OI_y": "CHG_IN_OI_N_1"},inplace=True)



for i in range(1,(Total_Number_of_Files-1)):
    #print("sushen {}".format(i))
    df_semi[i] = pd.merge(df_semi[i-1],df_Contracts_and_OIchange_slice[i+1],on="SYMBOL",how="left")
    df_semi[i].rename(columns={"CONTRACTS": "CONTRACTS_N_{}".format(i+1), "CHG_IN_OI": "CHG_IN_OI_N_{}".format(i+1)},inplace=True)


full_list = []

for index,row in df_semi[(Total_Number_of_Files-2)].iterrows():

    sub_list = []
    sub_list.append(row["SYMBOL"])

    sub_list.append(row["CHG_IN_OI_N"])
    for i in  range(2,Total_Number_of_Files):
        sub_list.append(row["CHG_IN_OI_N_{0}".format(i)])

    sub_list.append(row["CONTRACTS_N"])
    for i in range(2, Total_Number_of_Files):
        sub_list.append(row["CONTRACTS_N_{0}".format(i)])

    full_list.append(sub_list)


total_number_of_elemnts = len(full_list)
for index in range(total_number_of_elemnts):
    stock_specific_data = full_list[index]


    if(stock_specific_data[0] == 'AXISBANK'):
        print(stock_specific_data)
        #sliced_data = stock_specific_data[(Total_Number_of_Files-1):(len(stock_specific_data)+1)]
        sliced_data = stock_specific_data[1:(Total_Number_of_Files+1)]
        #print("{0}:{1}".format((Total_Number_of_Files),(len(stock_specific_data)+1)))

date_list = []
for i in range(len(FilePath)):
    date_list.append(FilePath[i].split("\\")[4].lstrip("fo").rstrip("bhav.csv").rstrip("2020"))

dates = date_list[::-1]
Open_interest = sliced_data
Open_interest = Open_interest[::-1]


plt.ylabel('Change in Open Interest : Futures Derivatives')
plt.xlabel('Date on which Data Recorded')
plt.title("Trend of Change in Open Interest in Futures Data")
plt.xticks(rotation = 55)
plt.legend(['SKDA'])
plt.plot(dates,Open_interest)
plt.grid(True,color='k',linestyle=':')


plt.show()




