import pandas as pd
from FinanceDataScrapping.DataBaseOperations import DataBase_ReadOperation
from FinanceDataScrapping.DataBaseOperations import DataBase_WriteOperation

writeProcess = DataBase_WriteOperation.Write_operation()


FilePath = [ "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo23JUL2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo22JUL2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo21JUL2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo20JUL2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo17JUL2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo16JUL2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo15JUL2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo14JUL2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo10JUL2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo09JUL2020bhav.csv",
            "D:\\Swing Trading Data\\EOD_Data_For_Swing_Trade\\Change_In_OpenInterest\\fo08JUL2020bhav.csv",
        ]


df_futures = [None]*11
df_Contracts_and_OIchange_complete = [None]*11
df_Contracts_and_OIchange_slice = [None]*11

for file_index in range(0,11):
    #print(FilePath[file_index])
    df_futures[file_index] = pd.read_csv(FilePath[file_index])

    #df_Contracts_and_OIchange_complete[file_index]= df_futures[file_index][(df_futures[file_index]['INSTRUMENT'] == 'FUTSTK') & (df_futures[file_index]['EXPIRY_DT'] == '28-May-2020')]
    #df_Contracts_and_OIchange_complete[file_index] = df_futures[file_index][(df_futures[file_index]['INSTRUMENT'] == 'FUTSTK') & (df_futures[file_index]['EXPIRY_DT'] == '25-Jun-2020')]
    df_Contracts_and_OIchange_complete[file_index] = df_futures[file_index][(df_futures[file_index]['INSTRUMENT'] == 'FUTSTK') & (df_futures[file_index]['EXPIRY_DT'] == '30-Jul-2020')]
    df_Contracts_and_OIchange_slice[file_index]  = df_Contracts_and_OIchange_complete[file_index][['SYMBOL', 'CONTRACTS', 'CHG_IN_OI','CLOSE','OPEN_INT']]
    print(df_Contracts_and_OIchange_slice[file_index].head())

df_semi = [None]*11
df_semi[0] = pd.merge(df_Contracts_and_OIchange_slice[0],df_Contracts_and_OIchange_slice[1],on="SYMBOL",how="left")
df_semi[0].rename(columns={"CONTRACTS_x": "CONTRACTS_N", "CHG_IN_OI_x": "CHG_IN_OI_N","CONTRACTS_y": "CONTRACTS_N_1", "CHG_IN_OI_y": "CHG_IN_OI_N_1"},inplace=True)
#print(df_semi[0].columns)


for i in range(1,10):
    #print("sushen {}".format(i))
    df_semi[i] = pd.merge(df_semi[i-1],df_Contracts_and_OIchange_slice[i+1],on="SYMBOL",how="left")
    df_semi[i].rename(columns={"CONTRACTS": "CONTRACTS_N_{}".format(i+1), "CHG_IN_OI": "CHG_IN_OI_N_{}".format(i+1)},inplace=True)
    #print(df_semi[i].columns)


print(df_semi[9].columns)
#sub_list = []
full_list = []
for index,row in df_semi[9].iterrows():
    sub_list = []
    sub_list.append(row["SYMBOL"])

    sub_list.append(row["CHG_IN_OI_N"])
    sub_list.append(row["CHG_IN_OI_N_1"])
    sub_list.append(row["CHG_IN_OI_N_2"])
    sub_list.append(row["CHG_IN_OI_N_3"])
    sub_list.append(row["CHG_IN_OI_N_4"])
    sub_list.append(row["CHG_IN_OI_N_5"])
    sub_list.append(row["CHG_IN_OI_N_6"])
    sub_list.append(row["CHG_IN_OI_N_7"])
    sub_list.append(row["CHG_IN_OI_N_8"])
    sub_list.append(row["CHG_IN_OI_N_9"])
    sub_list.append(row["CHG_IN_OI_N_10"])

    sub_list.append(row["CONTRACTS_N"])
    sub_list.append(row["CONTRACTS_N_1"])
    sub_list.append(row["CONTRACTS_N_2"])
    sub_list.append(row["CONTRACTS_N_3"])
    sub_list.append(row["CONTRACTS_N_4"])
    sub_list.append(row["CONTRACTS_N_5"])
    sub_list.append(row["CONTRACTS_N_6"])
    sub_list.append(row["CONTRACTS_N_7"])
    sub_list.append(row["CONTRACTS_N_8"])
    sub_list.append(row["CONTRACTS_N_9"])
    sub_list.append(row["CONTRACTS_N_10"])

    full_list.append(sub_list)


print(full_list)

writeProcess.insertFutureData_for_N_Month(full_list)
#writeProcess.insertFutureData_for_N_plus_1_Month(full_list)
#writeProcess.insertFutureData_for_N_plus_2_Month(full_list)


